﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Mapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using StructureMap;


namespace FluentNHibernateRepository
{
    /// <summary>
    /// EntityBase represents objects primary defined by it's identity (Id).
    /// The type of the identity, usually but not necessariily is of 
    /// type integer and is automatically assigned primary key.
    /// 
    /// All of the domain entities inherit this base class.
    /// All of the domain entities have an identity and are able
    /// to state if the data they contain is valid.
    /// 
    /// To check if the entity object contains valid data, they expose 
    /// property named Approval which is a concrete implementation of
    /// ApprovalBase. The entity's Validate() method just wraps
    /// the Approval.Validate(). The validation of entity returns list 
    /// of error messages and there is also a shortcut methnod IsValid()
    /// which just gives back true if the validation was successful 
    /// or false if the validation has failed.
    /// 
    /// The entity objects implement IComparable interface and equality tests.
    /// The have Equals() method to check if two variables point to same instance 
    /// of the same entity class and also they have equality operators that check
    /// if the IDs are same.
    /// </summary>
    public abstract class EntityBase<TIdentity> where TIdentity : IComparable
    {
        // Gets or sets the identity.
        public virtual TIdentity Id { get; set; }

        protected EntityBase(TIdentity Id) { this.Id = Id; }

        protected EntityBase() { }

        // Approval object that does the validation.
        public abstract ApprovalBase Approval { get; set; }

        // Validation
        public virtual IList<String> Validate()
        {
            if (Approval != null)
                return Approval.Validate(this);
            else
                return new List<String>();
        }

        // Validation shortcut
        public virtual bool IsValid()
        {
            return (Validate().Count == 0);
        }


        // Equality tests /////////////

        // Determines whether the specified entity parameter
        /// is equal to the current object (is the same reference)
        public override bool Equals(object entity)
        {
            if (entity == null || !(entity is EntityBase<TIdentity>))
                return false;

            return (this == (EntityBase<TIdentity>)entity);
        }

        // Check if two entities have same ID
        public static bool operator ==(EntityBase<TIdentity> entity1, EntityBase<TIdentity> entity2)
        {

            object obj1 = entity1 as object;
            object obj2 = entity2 as object;

            if ((obj1 == null) && (obj2 == null))
                return true;

            if ((obj1 == null) || (obj2 == null))
                return false;

            if (entity1.GetType() != entity2.GetType())
                return false;

            if (entity1.Id.CompareTo(entity2.Id) != 0)
                return false;

            return true;
        }

        // Check if two entities have different ID
        public static bool operator !=(EntityBase<TIdentity> entity1, EntityBase<TIdentity> entity2)
        {
            return (!(entity1 == entity2));
        }

        // GetHashCode() if the Id.
        public override int GetHashCode() { return this.Id.GetHashCode(); }
    }


    /// <summary>
    /// The entity objects have property based
    /// dependency injection with approval object.
    /// 
    /// If the entity has approval object,
    /// the entity can be validated to prevent saving
    /// entities with incorrect values, or to inform the 
    /// end user that the data he has entered is invalid.
    /// 
    /// The validation can be changed according to the context
    /// the entity is in. In one situation the entity may be valid, but
    /// in other situation the same entity with same properties
    /// may not be valid.
    /// 
    /// The ApprovalBase is an abstract class from wich the concrete approval
    /// classes will inherit. The ApprovalBase has method Validate wich returns 
    /// List of ApprovalErrorMessage error messages.
    /// </summary>
    public abstract class ApprovalBase
    {
        public abstract IList<String> Validate<TEntity>(TEntity entity);
    }


    /// Between the domain and data mapping layers a repository object 
    /// is used to implement all the quering logic. This is good, as 
    /// from one side we have all the query related logic in one place
    /// and makes support easier, from other side we can use dependency 
    /// injection to switch to fake implementation of repository at testing.
    /// 
    /// I found the ideas from Russell East’s blog "Implementing the Repository and Finder patterns"
    /// to be very promising and I think it is a very advanced way of implementing repositories.
    /// 
    /// For the original idea, please refer to
    /// http://russelleast.wordpress.com/2008/09/20/implementing-the-repository-and-finder-patterns/
    /// 
    /// I'm just follow the same pattern here. The quering repositories depend on 
    /// finder and ance repository implementations.
    /// 
    /// 



    /// <summary>
    /// All the entity finders extend a base IEntityFinder interface.
    /// The generic type is constrained by the EntityBase type.
    /// 
    /// The IEntityFinder interface is not to be used directly,
    /// but to be extended by typed Repository interfaces.
    /// </summary>
    public interface IFinder<TEntity, TIdentity>
        where TEntity : EntityBase<TIdentity>
        where TIdentity : IComparable
    {
        // Has a DataSource property of IQueryable on which 
        // we use LINQ for quering.
        IQueryable<TEntity> DataSource { get; set; }

        /// Get count of objects/records.
        int Count();

        /// Find by id. We have entityConcrete = repositoryConcrete.Find.ById(123) like usage.
        TEntity ById(TIdentity id);
    }


    /// <summary>
    /// Finder objects implement search logic/functions using LINQ.
    /// The classes inherited from EntityRepository have then object Find, 
    /// which method names by convention look like "ById", "ByName", etc.
    /// The concrete finders have to implement at least ById method.
    /// </summary>
    public abstract class FinderBase<TEntity, TIdentity>
        : IFinder<TEntity, TIdentity>
        where TEntity : EntityBase<TIdentity>
        where TIdentity : IComparable
    {
        public IQueryable<TEntity> DataSource { get; set; }

        /// Gets count of records.
        public int Count()
        {
            return DataSource.Count();
        }

        /// Find one entity by ID.
        public abstract TEntity ById(TIdentity id);
    }



    /// <summary>
    /// The concrete the ORM specific implementation is abstracted with interface IPersistor.
    /// The repository takes care for quering, the persistance repository takes care for persistance
    /// and is ORM specific implementation.
    /// We can then switch between NHibernate, Entity Framework, whatever for our needs.
    /// 
    /// The basic operations in the contract declared by this interface
    /// are: get entity by id; save entity to persist; delete entity from persist repository;
    /// get the query object on top of wich to implement concrete LINQ queries.
    /// </summary>
    public interface IPersistor<TEntity, TIdentity>
    {
        TEntity Get(TIdentity id);
        void Save(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> Query();
    }



    /// <summary>
    /// All the repositories extend a base IRepository interface.
    /// The generic type is restricted to be EntityBase type.
    /// 
    /// The basic operations in the contract declared by this interface
    /// are: get entity by id; save entity to persist; delete entity from persist repository.
    /// A query object is not directly exposed as interface as the conctere repositories
    /// should capculate the concrete queries and keep the internal query object hiden from outside.
    /// </summary>
    public interface IRepository<TEntity, TIdentity>
        where TEntity : EntityBase<TIdentity>
        where TIdentity : IComparable
    {
        TEntity Get(TIdentity id);
        IList<String> Save(TEntity entity);
        void Delete(TEntity entity);
        // hidden // IQueryable<TEntity> Query();
    }

    /// <summary>
    /// The RepositoryBase is an abstract class in which all repositories inherit from.
    /// The repositories are injected with concrete entity types using the typed 
    /// interface, so we have one repository per entity.
    /// 
    /// This repository class must be supplied an instance of IPersistor.
    /// This repository class must be supplied an instance of IFinder.
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TIdentity"></typeparam>
    public abstract class RepositoryBase<TEntity, TIdentity>
        : IRepository<TEntity, TIdentity>
        where TEntity : EntityBase<TIdentity>
        where TIdentity : IComparable
    {
        private readonly IPersistor<TEntity, TIdentity> persistor;
        public IPersistor<TEntity, TIdentity> Persistor { get { return persistor; } }

        private readonly IFinder<TEntity, TIdentity> finder;
        public IFinder<TEntity, TIdentity> Finder { get { return finder; } }

        private RepositoryBase() { }

        public RepositoryBase(
            IPersistor<TEntity, TIdentity> persistor,
            IFinder<TEntity, TIdentity> finder)
        {
            this.persistor = persistor;
            this.finder = finder;
            this.finder.DataSource = Query();
        }

        // Get entity by ID
        public virtual TEntity Get(TIdentity id)
        {
            return persistor.Get(id);
        }

        /// <summary>
        /// Validate and Save the entity. If the validation failed, will not save the entity,
        /// but returns back list of error messages.
        /// </summary>
        public virtual IList<String> Save(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            IList<String> errors = entity.Validate();

            if (errors.Count == 0)
                persistor.Save(entity);

            return errors;
        }

        // Delete entity from persistance repository
        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            persistor.Delete(entity);
        }

        /// Gets IQueryable which we use from the concrete
        /// implementation of repository to implement our 
        /// query methods (FindBy).
        protected IQueryable<TEntity> Query()
        {
            return persistor.Query();
        }
    }







    ///////////////// Book Store ///////////////////////////////////////////////////




    /// <summary>
    /// Book entity have along with title, publisher info, ISBN also a list of one or many authors.
    /// </summary>
    public class Book : EntityBase<int>
    {
        public Book() : base() 
        {
            this.Authors = new List<Author>();
        }
        public Book(int id) : this() { Id = id; }
        public override ApprovalBase Approval { get; set; }

        public virtual string Title { get; set; }
        public virtual string Publisher { get; set; }
        public virtual string ISBN { get; set; }

        public virtual IList<Author> Authors { get; private set; }
    }

    /// <summary>
    /// An author have a name and also eventually takes part on writing several books.
    /// </summary>
    public class Author : EntityBase<int>
    {
        public Author() : base() 
        {
            this.Books = new List<Book>();
        }
        public Author(int id) : this() { Id = id; }
        public override ApprovalBase Approval { get; set; }

        public virtual string Name { get; set; }
        public virtual IList<Book> Books { get; private set; }
    }



    /// <summary>
    /// To check if Book object contains valid data we implement BookApproval.
    /// A Book should have title and at least one author.
    /// </summary>
    public class BookApproval : ApprovalBase
    {
        public override IList<String> Validate<TEntity>(TEntity entity)
        {
            List<String> errors = new List<String>();
            Book book = entity as Book;

            if (book == null)
                throw new ArgumentException("Entity not a book");

            if (String.IsNullOrEmpty(book.Title))
                errors.Add("Title can not be empty");

            if ((book.Authors == null) || (book.Authors.Count == 0))
                errors.Add("A book should have at least one author");

            return errors;
        }
    }

    /// <summary>
    /// To check if Author object contains valid data we implement AuthorApproval.
    /// An Author should have name.
    /// </summary>
    public class AuthorApproval : ApprovalBase
    {
        public override IList<String> Validate<TEntity>(TEntity entity)
        {
            List<String> errors = new List<String>();
            Author author = entity as Author;

            if (author == null)
                throw new ArgumentException("Entity is not an author");

            if (String.IsNullOrEmpty(author.Name))
                errors.Add("Name can not be empty");

            return errors;
        }
    }


    /////////////////////////////// Finder and Repository ////////////////////////////



    public interface IBookFinder : IFinder<Book, int> 
    {
        Book ByISBN(string ISBN); // adds new find methiod
    }
    public interface IBookRepository : IRepository<Book, int> 
    { 
        IBookFinder Find { get; }
    }


    public interface IAuthorFinder : IFinder<Author, int> 
    {
        Author ByName(string name);
    }
    public interface IAuthorRepository : IRepository<Author, int>
    { 
        IAuthorFinder Find { get; } 
    }


    public class BookFinder : FinderBase<Book, int>, IBookFinder
    {
        public override Book ById(int id)
        {
            return DataSource.Where(b => b.Id == id).FirstOrDefault();
        }
        public Book ByISBN(string ISBN)
        {
            return DataSource.Where(b => b.ISBN == ISBN).FirstOrDefault();
        }
    }

    public class BookRepository : RepositoryBase<Book, int>, IBookRepository
    {
        public BookRepository(
            IPersistor<Book, int> persistor,
            IBookFinder entityFinder)
            : base(persistor, entityFinder) { }

        public IBookFinder Find
        {
            get { return (IBookFinder)Finder; }
        }
    }

    public class AuthorFinder : FinderBase<Author, int>, IAuthorFinder
    {
        public override Author ById(int id)
        {
            return DataSource.Where(a => a.Id == id).FirstOrDefault();
        }
        public Author ByName(string name)
        {
            return DataSource.Where(a => a.Name == name).FirstOrDefault();
        }
    }

    public class AuthorRepository : RepositoryBase<Author, int>, IAuthorRepository
    {
        public AuthorRepository(
            IPersistor<Author, int> persistor,
            IAuthorFinder finder)
            : base(persistor, finder) { }

        public IAuthorFinder Find
        {
            get { return (IAuthorFinder)Finder; }
        }
    }






    ////////////////// Fluent NHibernate //////////////////////////////////////////////


    public sealed class BooksMap : ClassMap<Book>
    {
        public BooksMap()
        {
            Id(x => x.Id);
            Map(x => x.Title).Length(256).Default("").Not.Nullable();
            Map(x => x.Publisher).Length(256).Default("").Nullable();
            Map(x => x.ISBN).Length(16).Default("").Not.Nullable();
            HasManyToMany(x => x.Authors).LazyLoad();
        }
    }
    public sealed class AuthorsMap : ClassMap<Author>
    {
        public AuthorsMap()
        {
            Id(x => x.Id);
            Map(x => x.Name).Length(128).Default("").Not.Nullable();
            HasManyToMany(x => x.Books).LazyLoad();
        }
    }







    //////////////////////////// NHibernate persistance /////////////////////////////////////



    // Concrete NHibernate based persistance repository.
    public abstract class PersistorBase<TEntity, TIdentity>
        : IPersistor<TEntity, TIdentity>
    {
        protected ISession Session { get; set; }

        public PersistorBase(ISession session)
        {
            Session = session;
        }

        public TEntity Get(TIdentity id)
        {
            return (TEntity)Session.Get(typeof(TEntity), id);
        }

        public void Save(TEntity entity)
        {
            Session.SaveOrUpdate(entity);
        }

        public void Delete(TEntity entity)
        {
            Session.Delete(entity);
        }

        public IQueryable<TEntity> Query()
        {
            var qry = from t in Session.Linq<TEntity>()
                      select t;

            return qry.AsQueryable();
        }
    }

    // Persistance repository for books.
    public class BookPersistor : PersistorBase<Book, int>
    {
        public BookPersistor(ISession session) : base(session) { }
    }

    // Persistance repository for authors.
    public class AuthorPersistor : PersistorBase<Author, int>
    {
        public AuthorPersistor(ISession session) : base(session) { }
    }




    /// BaseDatabase is wrapper class for NHibernate initialization - a class from which concrete 
    /// per database implementations will be created.
    /// From this one we create implemebntation for Posgres, for MySQL, for SQLite, etc.
    /// Into the concrete implementations we switch drivers, connection string, etc.
    public abstract class BaseDatabase : IDisposable
    {
        protected static Configuration configuration;
        protected static ISessionFactory sessionFactory;

        public ISession Session { get; protected set; }

        /// Does initialization, run from the constructors of the inherited classes.
        protected void Init()
        {
            sessionFactory = CreateSessionFactory();
            Session = sessionFactory.OpenSession();
            BuildSchema(Session);
        }

        protected abstract ISessionFactory CreateSessionFactory();

        private static void BuildSchema(ISession session)
        {
            SchemaExport schemaExport = new SchemaExport(configuration);
            schemaExport.Execute(true, true, false, session.Connection, null);
        }

        public void Dispose()
        {
            Session.Dispose();
        }

    }


    /// Database configured as in file SQLite database.
    public class SQLiteInFileDatabase : BaseDatabase
    {
        public string FileName { get; private set; }

        /// Create SQLite database with specified file name.
        public SQLiteInFileDatabase(string fileName)
        {
            // file name to store the database, "database.db" for example.
            FileName = fileName;

            // does not run the base constructor, but calls Init() by itself.
            Init(); 
        }

        protected override ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.UsingFile(FileName).ShowSql())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<BooksMap>())
                .ExposeConfiguration(Cfg => configuration = Cfg)
                .BuildSessionFactory();
        }
    }

    /// Database configured as in memory SQLite database.
    public class SQLiteInMemoryDatabase : BaseDatabase
    {
        /// Create in memory SQLite database.
        public SQLiteInMemoryDatabase()
        {
            Init();
        }

        protected override ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.InMemory())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<BooksMap>())
                .ExposeConfiguration(Cfg => configuration = Cfg)
                .BuildSessionFactory();
        }
    }


    /// Database configured as in memory SQLite database.
    public class PostgresDatabase : BaseDatabase
    {
        public string ConnectionString { get; private set; }

        /// Create in memory SQLite database.
        public PostgresDatabase(string connectionString)
        {
            ConnectionString = connectionString;
            Init();
        }

        protected override ISessionFactory CreateSessionFactory()
        {
            return Fluently.Configure()
                .Database(PostgreSQLConfiguration.PostgreSQL81
                    .ConnectionString(ConnectionString).ShowSql())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<BooksMap>())
                .ExposeConfiguration(Cfg => configuration = Cfg)
                .BuildSessionFactory();
        }
    }







    ////////////////// BookStore database class ////////////////////////////////////



    /// Is a singleton, use GetInstance()
    public class BookStore
    {
        private static BookStore bookStore;

        public static BookStore GetInstance()
        {
            if (bookStore == null)
                bookStore = new BookStore();
            return bookStore;
        }

        private BaseDatabase db;
        public IBookRepository BookRepository { get; private set; }
        public IAuthorRepository AuthorRepository { get; private set; }


        /// Inject concrete instance of BaseDatabase.
        public BookStore()
            : this(ObjectFactory.GetInstance<BaseDatabase>())
        { }

        public BookStore(BaseDatabase db)
        {
            this.db = db;
            Init();
        }


        private void Init()
        {
            IBookFinder bookFinder = new BookFinder();
            BookPersistor bookPersistor = new BookPersistor(db.Session);
            BookRepository = new BookRepository(bookPersistor, bookFinder);


            IAuthorFinder authorFinder = new AuthorFinder();
            AuthorPersistor authorPersistor = new AuthorPersistor(db.Session);
            AuthorRepository = new AuthorRepository(authorPersistor, authorFinder);
        }

        // can be used to implement the Unit of Work Pattern.
        public void Flush()
        {
            db.Session.Flush();
        }
    }


    //////////////////////////// Usage ////////////////////////////////////

    public class Program
    {
        public class SQLiteInFileConcreteDatabase : SQLiteInFileDatabase
        {
            public SQLiteInFileConcreteDatabase() : base("test.db") { }
        }

        public class PostgresConcreteDatabase : PostgresDatabase
        {
            public PostgresConcreteDatabase()
                : base("Server=127.0.0.1;Port=5432;Database=bookstore;User Id=postgres;Password=postDba;")
            { }
        }

        /// <summary>
        /// Configure StructureMap dependencies.
        /// </summary>
        private static void ConfigureDependencies()
        {
            ObjectFactory.Initialize(x =>
            {
                x.ForRequestedType<BaseDatabase>()
                    .TheDefaultIsConcreteType<SQLiteInMemoryDatabase>();
                    // .TheDefaultIsConcreteType<SQLiteInFileDatabaseConcrete>();
                    // .TheDefaultIsConcreteType<PostgresConcreteDatabase>();
                    // whatever concrete database and settings
            });
        }



        static void Main(string[] args)
        {
            ConfigureDependencies();

            BookStore bookStore = BookStore.GetInstance();

            Author JohnDoe = new Author() { Name = "John Doe" };
            bookStore.AuthorRepository.Save(JohnDoe);

            Author JaneDoe = new Author() { Name = "Jane Doe" };
            bookStore.AuthorRepository.Save(JaneDoe);

            Book book;

            // test approval:
            book = new Book() { };
            book.Approval = new BookApproval();
            Console.WriteLine("Approval of book: {0}", String.Join(System.Environment.NewLine, book.Validate().ToArray()));

            book = new Book() { Title = "Meet John Doe", ISBN = "1234567890123" };
            book.Authors.Add(JohnDoe);
            book.Authors.Add(JaneDoe);
            bookStore.BookRepository.Save(book);

            bookStore.Flush();

            Book selected = bookStore.BookRepository.Find.ByISBN("1234567890123");
            Console.WriteLine(String.Format("Author one: {0}", selected.Authors[0].Name));
            Console.WriteLine(String.Format("Author two: {0}", selected.Authors[1].Name));
            Console.ReadLine();
        }


    }

    // Generated SQL for SQLite in memory

    //drop table if exists "Book"

    //drop table if exists BooksToAuthors

    //drop table if exists "Author"

    //create table "Book" (
    //    Id  integer,
    //   Title TEXT not null,
    //   Publisher TEXT,
    //   ISBN TEXT not null,
    //   primary key (Id)
    //)

    //create table BooksToAuthors (
    //    Book_id INTEGER not null,
    //   Author_id INTEGER not null
    //)

    //create table "Author" (
    //    Id  integer,
    //   Name TEXT not null,
    //   primary key (Id)
    //)

    // NHibernate: INSERT INTO "Author" (Name) VALUES (@p0); select last_insert_rowid() ;@p0 = 'John Doe'
    // NHibernate: INSERT INTO "Author" (Name) VALUES (@p0); select last_insert_rowid() ;@p0 = 'Jane Doe'
    // NHibernate: INSERT INTO "Book" (Title, Publisher, ISBN) VALUES (@p0, @p1, @p2);
    //    -- select last_insert_rowid();@p0 = 'Meet John Doe', @p1 = NULL, @p2 = '1234567890123'
    // NHibernate: INSERT INTO BooksToAuthors (Book_id, Author_id) VALUES (@p0, @p1);@p0 = 1, @p1 = 1
    //

}
